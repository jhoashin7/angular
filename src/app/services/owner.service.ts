import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Owner } from './../interfaces/owner';
import { WHITE_ON_BLACK_CSS_CLASS } from '@angular/cdk/a11y/high-contrast-mode/high-contrast-mode-detector';
import { from } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class OwnerService {

  constructor(private http: HttpClient) {}
    getOwner(){
      return this.http.get<Owner[]>('http://localhost:8000/api/owner');
    }
}
