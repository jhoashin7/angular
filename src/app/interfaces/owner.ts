export interface Owner {
    id          :   number,
    card        :   number,
    name        :   string,
    last_name   :   string,
}

export class OwnerObject {
    id:number
    card:number 
    name:string
    last_name:string
    constructor(id:number, card:number, name:string, last_name:string){
        this.id = id
        this.card = card
        this.name = name
        this.last_name = last_name
    }
}
