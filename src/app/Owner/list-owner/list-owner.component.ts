import { Component, OnInit } from '@angular/core';
import {OwnerService} from './../../services/owner.service';
import { Owner, OwnerObject } from 'src/app/interfaces/owner';
import { MatDialog } from '@angular/material/dialog';
import { CreateOwnerComponent } from '../create-owner/create-owner.component';

@Component({
  selector: 'app-list-owner',
  templateUrl: './list-owner.component.html',
  styleUrls: ['./list-owner.component.css']
})
export class ListOwnerComponent implements OnInit {
  displayedColumns: string[] = ['id', 'card', 'name', 'last_name','option'];
  private owerSelected:Owner;
  data : Owner[] = [
    new OwnerObject(1, 1, 'juan', 'suarez'),
    new OwnerObject(2, 2, 'juan', 'suarez'),
    new OwnerObject(3, 3, 'juan', 'suarez')
  ];

  constructor(private ownerService: OwnerService,public dialog: MatDialog) { }

  selectOwnerToUpdate(ownerSelected:Owner) {
    //console.log(ownerSelected)
    const dialogRef = this.dialog.open(CreateOwnerComponent, {
      width: '500px',
      data: ownerSelected
    });
    dialogRef.afterClosed().subscribe((ownerUpdated:Owner) => {
      //aqui puede tener el usuario que se tiene del form
      console.log(ownerUpdated)
    })
  }

  ngOnInit(){
    this.ownerService.getOwner()
      .subscribe(
        owners => {
          this.data = owners
          //console.log(owners)
        },
        err =>console.log(err)
      )
  }



}
