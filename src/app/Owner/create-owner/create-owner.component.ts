import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Owner } from 'src/app/interfaces/owner';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-create-owner',
  templateUrl: './create-owner.component.html',
  styleUrls: ['./create-owner.component.css']
})
export class CreateOwnerComponent implements OnInit {

  public formCreateOwner:FormGroup;
  constructor(
    private fb:FormBuilder,
    private dialogRef:MatDialogRef<CreateOwnerComponent>,
    @Inject(MAT_DIALOG_DATA) public owner:Owner 
  ) { }

  ngOnInit() {
    this.buildFormOwnerCreate()
  }

  public get name() {
    return this.formCreateOwner.get('name')
  }
  public get last_name() {
    return this.formCreateOwner.get('last_name')
  }
  public get card() {
    return this.formCreateOwner.get('card')
  }
  
  private buildFormOwnerCreate(): void {
    this.formCreateOwner = this.fb.group({
      name: this.fb.control(this.getValueKey('name'), Validators.compose([Validators.required])),
      last_name: this.fb.control(this.getValueKey('last_name'), Validators.compose([Validators.required])),
      card: this.fb.control(this.getValueKey('card'), Validators.compose([Validators.required, Validators.pattern(/^\d+$/gm)])),
      id: this.getValueKey('id'),
    })
  }

  private getValueKey(key:string): string | number {
    return this.owner ? this.owner[key] : ''
  }

  public onSubmit(): void {
    if(!this.formCreateOwner.invalid) {
      this.dialogRef.close(this.formCreateOwner.value)
    } else {
      this.formCreateOwner.markAllAsTouched()
    }
  }

  public onNoClick(): void {
    this.dialogRef.close()
  }
}
